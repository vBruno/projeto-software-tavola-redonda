/**
 *
 * @author tarcisio
 */
package controle;
import java.sql.SQLException; 
import java.sql.PreparedStatement; 
import java.sql.ResultSet;
import modelo.Aluguel;

public class AddAluguelC {
    public boolean registraAluno(Aluguel aluguel){
        Conexao con = new Conexao();
        
        try{
            con.abrirConexao();
            int livroId = 0;
            int alunoId = 0;
            
            PreparedStatement ps = con.getCon().prepareStatement("INSERT into Aluno (nome, turma) VALUES(?, ?);");
            ps.setString(1, aluguel.est.getNome());
            ps.setString(2, aluguel.est.getTurma());
            
            PreparedStatement ps2 = con.getCon().prepareStatement("INSERT into Livro (nome, autor) VALUES(?, ?);");
            ps2.setString(1, aluguel.book.getNome());
            ps2.setString(2, aluguel.book.getAutor());
            
            if(!ps.execute() && !ps2.execute()){
                PreparedStatement idBook = con.getCon().prepareStatement("SELECT id FROM Livro ORDER BY id DESC LIMIT 1;");
                PreparedStatement idEst = con.getCon().prepareStatement("SELECT id FROM Aluno ORDER BY id DESC LIMIT 1;");
                ResultSet rs1 = idBook.executeQuery();
                ResultSet rs2 = idEst.executeQuery();
            
                while(rs1.next()){
                    livroId = rs1.getInt("id");
                }
                while(rs2.next()){
                    alunoId = rs2.getInt("id");
                }
                PreparedStatement ps3 = con.getCon().prepareStatement("INSERT into Aluguel (locacao, devolucao, idL, idA) VALUES(?, ?, ?, ?);");
                ps3.setDate(1, aluguel.getLocacao());
                ps3.setDate(2, aluguel.getDevolucao());
                ps3.setInt(3, livroId);
                ps3.setInt(4, alunoId);
                if(!ps3.execute()){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        } catch(SQLException e){
            System.out.println("[-] Erro ao inserir informações");
            System.out.println(e.getMessage());
            return false;
        } finally{
            con.fecharConexao();
        }
    }
}

//update Aluno as al join Aluguel as alu on alu.idA = al.id set al.nome = "Eduarda" where alu.id = 2;

