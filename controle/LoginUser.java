package controle;
import modelo.User;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import controle.Conexao;
 
public class LoginUser{
   public boolean login(User user){
       Conexao con = new Conexao();
       try{
           con.abrirConexao();
 
           
           PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM Usuario WHERE nome=? AND senha=?;");
           ps.setString(1, user.getNome());
           ps.setString(2, user.getSenha());
 
           ResultSet rs = ps.executeQuery();
           if(rs.next()){
               return true;
           }else{
               return false;
           }
       }catch(SQLException e){
           System.out.println("Erro ao consultar o banco");
           System.out.println(e.getMessage());
           return false;
       }finally{
           con.fecharConexao();
       }
   }
}
