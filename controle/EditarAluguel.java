
package controle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.Aluguel;

/**
 *
 * @author eduardahey
 */
public class EditarAluguel {
    public boolean EditarAluguel(Aluguel aluguel){
    Conexao con = new Conexao();
    // update Aluno as al join Aluguel as alu join Livro as li on alu.idA = al.id and alu.idL = li.id set al.nome = "Eduarda2", al.turma = "2° Informática", li.nome = "Capitú", li.autor = "Dom Casmurro", alu.devolucao = "2022-06-30", alu.locacao = "2022-07-01"  where alu.id = 1;
        try{
            con.abrirConexao();
            int livroId = 0;
            int alunoId = 0;
            
            PreparedStatement ps = con.getCon().prepareStatement("update Aluno as al join Aluguel as alu join Livro as li on alu.idA = al.id and alu.idL = li.id set al.nome = ?, al.turma = ?, li.nome = ?, li.autor = ?, alu.devolucao = ?, alu.locacao = ? where alu.id = ?;");
            ps.setString(1, aluguel.est.getNome());
            ps.setString(2, aluguel.est.getTurma());
            ps.setString(3, aluguel.book.getNome());
            ps.setString(4, aluguel.book.getAutor());
            ps.setDate(5, aluguel.getDevolucao());
            ps.setDate(6, aluguel.getLocacao());
            ps.setInt(7, aluguel.getIdAlug());
            
            if(!ps.execute()){
                PreparedStatement idBook = con.getCon().prepareStatement("SELECT id FROM Livro ORDER BY id DESC LIMIT 1;");
                PreparedStatement idEst = con.getCon().prepareStatement("SELECT id FROM Aluno ORDER BY id DESC LIMIT 1;");
                ResultSet rs1 = idBook.executeQuery();
                ResultSet rs2 = idEst.executeQuery();
            
                while(rs1.next()){
                    livroId = rs1.getInt("id");
                }
                while(rs2.next()){
                    alunoId = rs2.getInt("id");
                }

                if(!ps.execute()){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        } catch(SQLException e){
            System.out.println("[-] Erro ao inserir informações");
            System.out.println(e.getMessage());
            return false;
        } finally{
            con.fecharConexao();
        }
    }
    public Aluguel consultar(int id){
        Conexao con = new Conexao();
        try{
            Aluguel alu = new Aluguel();
                con.abrirConexao();
                PreparedStatement ps = con.getCon().prepareStatement("select Aluno.nome, Aluno.turma, Livro.nome, Livro.autor, Aluguel.devolucao, Aluguel.locacao from Aluguel inner join Aluno on Aluno.id=Aluguel.idA inner join Livro on Livro.id=Aluguel.idL where Aluguel.id = ?;");
                ps.setInt(1, id);
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    alu.est.setNome(rs.getString("Aluno.nome"));
                    alu.book.setNome(rs.getString("Livro.nome"));
                    alu.book.setAutor(rs.getString("Livro.autor"));
                    alu.setDevolucao(rs.getDate("Aluguel.devolucao"));
                    alu.setLocacao(rs.getDate("Aluguel.locacao"));
                    alu.est.setTurma(rs.getString("Aluno.turma"));
                    return alu;
                }else{
                    return null;
                }
            } catch(SQLException e){
                System.out.println("[-] Erro ao inserir informações");
                System.out.println(e.getMessage());
            } finally{
                con.fecharConexao();
            }
            return null;
        }
    

}
