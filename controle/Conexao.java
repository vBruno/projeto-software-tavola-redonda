package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author bruno
 */
public class Conexao {
    private Connection con = null;
    
    public Connection getCon(){
        return this.con;
    }
    public void setCon(Connection c){
        if(c!=null){
            this.con = c;
        }
    }
    
    public boolean abrirConexao(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            String usuario = "brunin";
            String senha = "9054";
            String banco = "biblioteca";
            String host = "jdbc:mysql://localhost/"+banco;
            this.setCon(DriverManager.getConnection(host, usuario, senha));
            return true;
        }catch(ClassNotFoundException e){
            System.out.println("Erro: Arquivo de configuração não existe");
            System.out.println(e.getMessage());
            return false;
        }catch(SQLException e){
            System.out.println("Erro ao conectar no banco");
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    public boolean fecharConexao(){
        try{
            this.con.close();
            return true;
        }catch(SQLException e){
            System.out.println("Erro[-] Conexão não pode ser fechada");
            System.out.println(e.getMessage());
            return false;
        }
    }
    
}
