/**
 *
 * @author eduardahey and bruno
 */
package controle;
import java.sql.SQLException; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import modelo.Aluguel;

public class Alugueis{
    public Vector popularAlugados(java.sql.Date date1){
        Vector tb4 = new Vector();
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("select Aluno.nome AS aluno, Livro.nome AS livro, Aluguel.devolucao, Aluguel.id AS cod from Aluguel inner join Aluno on Aluno.id=Aluguel.idA inner join Livro on Livro.id=Aluguel.idL and Aluguel.devolucao>=?;");
            ps.setDate(1, date1);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Vector n3 = new Vector();
                n3.add(rs.getString("aluno"));
                n3.add(rs.getString("livro"));
                n3.add(rs.getString("devolucao"));
                n3.add(rs.getString("cod"));
                tb4.add(n3);
            }
            
        }catch(SQLException e){
            System.out.println(e.getMessage());
            
        }finally{
            con.fecharConexao();
        }
        return tb4;
    }
    
    /*public List<Aluguel> getAluguel(java.sql.Date date1){
        Conexao con = new Conexao();
        List<Aluguel> alugueis = new ArrayList<>();
        try{
            con.abrirConexao();
            //PreparedStatement ps = con.getCon().prepareStatement("SELECT * FROM Aluguel JOIN Aluno ON Aluguel.idA = Aluno.id Join Livro ON Aluguel.idL = Livro.id;");
            //ResultSet rs = ps.executeQuery();
            PreparedStatement ps = con.getCon().prepareStatement("select Aluno.nome AS aluno, Livro.nome AS livro, Aluguel.devolucao, Aluguel.id AS cod from Aluguel inner join Aluno on Aluno.id=Aluguel.idA inner join Livro on Livro.id=Aluguel.idL and Aluguel.devolucao<=?;");
            ps.setDate(1, date1);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Aluguel alug = new Aluguel();
                alug.setDevolucao(rs.getDate("devolucao"));
                alug.setLocacao(rs.getDate("locacao"));
                alug.setIdAlug(rs.getInt("id"));
                alug.est.setId(rs.getInt("idA"));
                alug.est.setNome(rs.getString("nome"));
                alug.est.setTurma(rs.getString("turma"));
                alug.book.setNome(rs.getString("Livro.nome"));
                alug.book.setAutor(rs.getString("Livro.Autor"));
                alug.book.setId(rs.getInt("Livro.Id"));
                alugueis.add(alug);
            }
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            con.fecharConexao();
        }
        return alugueis;
    }*/
    
    public boolean deletarAluguel(int codigo){
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("DELETE FROM Aluguel WHERE id=?");
            ps.setInt(1, codigo);
            if(!ps.execute()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println("Erro ao Excluir elemento");
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
        }
        return false;
    }
    
    public Vector PesquisarAlug(String pesq, java.sql.Date date1){
        Vector tb1 = new Vector();
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("select Aluno.nome AS aluno, Livro.nome AS livro, Aluguel.devolucao, Aluguel.id AS cod from Aluguel inner join Aluno on Aluno.id=Aluguel.idA inner join Livro on Livro.id=Aluguel.idL and Aluno.nome like ? and Aluguel.devolucao>?;");
            ps.setString(1, "%"+pesq+"%");
            ps.setDate(2, date1);
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Vector n1 = new Vector();
                n1.add(rs.getString("aluno"));
                n1.add(rs.getString("livro"));
                n1.add(rs.getString("devolucao"));
                n1.add(rs.getString("cod"));
                tb1.add(n1);
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
        }
        return tb1;
    }
    
    public Vector PesquisarExp(String pesq, java.sql.Date date1){
        Vector tb1 = new Vector();
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("select Aluno.nome AS aluno, Livro.nome AS livro, Aluguel.devolucao, Aluguel.id AS cod from Aluguel inner join Aluno on Aluno.id=Aluguel.idA inner join Livro on Livro.id=Aluguel.idL and Aluno.nome like ? and Aluguel.devolucao<=?;");
            ps.setString(1, "%"+pesq+"%");
            ps.setDate(2, date1);
            
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Vector n1 = new Vector();
                n1.add(rs.getString("aluno"));
                n1.add(rs.getString("livro"));
                n1.add(rs.getString("devolucao"));
                n1.add(rs.getString("cod"));
                tb1.add(n1);
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }finally{
            con.fecharConexao();
        }
        return tb1;
    }
    
    public Vector popularExpirados(java.sql.Date date1){
        Vector tb2 = new Vector();
        Conexao con = new Conexao();
        try{
            con.abrirConexao();
            PreparedStatement ps = con.getCon().prepareStatement("select Aluno.nome AS aluno, Livro.nome AS livro, Aluguel.devolucao, Aluguel.id AS cod from Aluguel inner join Aluno on Aluno.id=Aluguel.idA inner join Livro on Livro.id=Aluguel.idL and Aluguel.devolucao<=?;");
            ps.setDate(1, date1);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Vector n2 = new Vector();
                n2.add(rs.getString("aluno"));
                n2.add(rs.getString("livro"));
                n2.add(rs.getString("devolucao"));
                n2.add(rs.getString("cod"));
                tb2.add(n2);
            }
            
        }catch(SQLException e){
            System.out.println(e.getMessage());
            
        }finally{
            con.fecharConexao();
        }
        return tb2;
    }
    
    /*public static void Alug(){
        Alugueis aluguel = new Alugueis();
        for(Aluguel a : aluguel.getAluguel()){
        SimpleDateFormat formData = new SimpleDateFormat("dd/MM/yyyy");
        String dev = formData.format(a.getDevolucao());
        String loc = formData.format(a.getLocacao());
        System.out.println("Locação: " + loc);
        System.out.println("Devolução: " + dev);
        System.out.println("Id do Aluguel " + a.getIdAlug());
        System.out.println("Id do Aluno " + a.est.getId());
        System.out.println("Nome do Aluno " + a.est.getNome());
        System.out.println("Turma do Aluno " + a.est.getTurma());
        System.out.println("Nome do Livro " + a.book.getNome());
        System.out.println("Nome do Autor " + a.book.getAutor());
        System.out.println("Id do Livro " + a.book.getId());
        System.out.println("--------------------------------------------");
        }
    }*/
}
