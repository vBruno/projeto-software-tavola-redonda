package modelo;
import java.sql.Date; 
public class Aluguel{
    private Date locacao;
    private Date devolucao;
    private int idAlug;
    public Aluno est;
    public Livro book;
    
    public Aluguel() {
        this.est = new Aluno();
        this.book = new Livro();
    }
    
    public Date getLocacao() {
        return locacao;
    }

    public void setLocacao(Date locacao) {
        this.locacao = locacao;
    }

    public Date getDevolucao() {
        return devolucao;
    }

    public void setDevolucao(Date devolucao) {
        this.devolucao = devolucao;
    }

    public int getIdAlug() {
        return idAlug;
    }

    public void setIdAlug(int idAlug) {
        this.idAlug = idAlug;
    }
    
}