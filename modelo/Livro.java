package modelo;

public class Livro{
    private String nome;
    private String autor;
    private int id;
    
    public Livro(String nome, String autor, int id){
        this.nome = nome;
        this.autor = autor;
        this.id = id;
    } 
    public Livro(){
        
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
     
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
       
        }