package modelo;

public class Aluno{
    private String nome;
    private String turma;
    private int id;

    public Aluno(String nome,String turma, int matricula){
        this.nome = nome;
        this.turma = turma;
        this.id = matricula;
    }
    public Aluno(){
        
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }
    public void setId(int matricula) {
        this.id = matricula;
    } 
    public int getId() {
        return id;
    }
 
}