package utilitarios;
/**
 *
 * @author tarcisio
 */
public class VerificaDados {
    public boolean verificaVazio(Object... campo){ //este método verifica se algum campo está vazio.
        int verificador = 0;
        for(int i = 0; i < campo.length; i++){
            if(campo[i].equals("")){
                verificador = 1;
                break;
            }
        }
        if(verificador == 0){
            return true;
        }else{
            return false;
        }
    }
    
}
