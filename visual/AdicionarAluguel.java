package visual;
import javax.swing.JOptionPane;
import controle.AddAluguelC;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import modelo.Aluguel;
import utilitarios.VerificaDados;

/**
 *
 * @author tarcisio and lavigne
 */
public class AdicionarAluguel extends javax.swing.JInternalFrame {

    private TelaInicial anterior;
    public AdicionarAluguel(TelaInicial anterior) {
        initComponents();
        PrncCmpAluno.setVisible(false);
        PrncCmpTitleLivro.setVisible(false);
        PrncCmpAutorLivro.setVisible(false);
        PrncCmpdataAlug.setVisible(false);
        PrncCmpdataDevol.setVisible(false);
        PrncCmpTurma.setVisible(false);
        this.anterior = anterior;
    }
    
    AddAluguelC add = new AddAluguelC();
    Aluguel alug = new Aluguel();
    VerificaDados verifDados = new VerificaDados();
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnFundo = new javax.swing.JPanel();
        jlblNomeAluno = new javax.swing.JLabel();
        jlblTurma = new javax.swing.JLabel();
        jlblTituloLivro = new javax.swing.JLabel();
        jlblDataAluguel = new javax.swing.JLabel();
        jlblDataDevolucao = new javax.swing.JLabel();
        jtxtfNomeAluno = new javax.swing.JTextField();
        jtxtfTituloLivro = new javax.swing.JTextField();
        jformattedtxtfDataDevolucao = new javax.swing.JFormattedTextField();
        jformattedtxtfDataAluguel = new javax.swing.JFormattedTextField();
        btnSalvar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        jlblTitulo = new javax.swing.JLabel();
        jcboxTurma = new javax.swing.JComboBox<>();
        jlblAutorLivro = new javax.swing.JLabel();
        jtxtfAutorLivro = new javax.swing.JTextField();
        PrncCmpAluno = new javax.swing.JLabel();
        PrncCmpAutorLivro = new javax.swing.JLabel();
        PrncCmpdataDevol = new javax.swing.JLabel();
        PrncCmpdataAlug = new javax.swing.JLabel();
        PrncCmpTitleLivro = new javax.swing.JLabel();
        PrncCmpTurma = new javax.swing.JLabel();

        setBorder(null);
        setTitle("Registrar aluguel de livro");

        jpnFundo.setBackground(new java.awt.Color(239, 253, 251));
        jpnFundo.setMaximumSize(new java.awt.Dimension(1024, 500));
        jpnFundo.setMinimumSize(new java.awt.Dimension(1024, 500));

        jlblNomeAluno.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblNomeAluno.setText("Nome do Aluno:");

        jlblTurma.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblTurma.setText("Turma:");

        jlblTituloLivro.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblTituloLivro.setText("Título do Livro:");

        jlblDataAluguel.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblDataAluguel.setText("Data de Aluguel:");

        jlblDataDevolucao.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblDataDevolucao.setText("Data de Devolução:");

        jtxtfNomeAluno.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfNomeAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfNomeAlunoActionPerformed(evt);
            }
        });

        jtxtfTituloLivro.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfTituloLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfTituloLivroActionPerformed(evt);
            }
        });

        jformattedtxtfDataDevolucao.setBackground(new java.awt.Color(204, 204, 204));
        try {
            jformattedtxtfDataDevolucao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-##-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jformattedtxtfDataAluguel.setBackground(new java.awt.Color(204, 204, 204));
        try {
            jformattedtxtfDataAluguel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-##-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jformattedtxtfDataAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jformattedtxtfDataAluguelActionPerformed(evt);
            }
        });

        btnSalvar.setBackground(new java.awt.Color(90, 207, 184));
        btnSalvar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnSalvar.setForeground(new java.awt.Color(255, 255, 255));
        btnSalvar.setText("Salvar");
        btnSalvar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnFechar.setBackground(new java.awt.Color(90, 207, 184));
        btnFechar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnFechar.setForeground(new java.awt.Color(255, 255, 255));
        btnFechar.setText("Fechar");
        btnFechar.setToolTipText("");
        btnFechar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        jlblTitulo.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
        jlblTitulo.setText("Registrar Aluguel de Livro:");

        jcboxTurma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecionar", "1º Informática", "1º  Enfermagem", "1º Guia de Turismo", "2º Informática", "2º Enfermagem", "2º Guia de Turismo", "3º Informática", "3º Enfermagem", "3º Guia de Turismo" }));
        jcboxTurma.setToolTipText("");
        jcboxTurma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcboxTurmaActionPerformed(evt);
            }
        });

        jlblAutorLivro.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblAutorLivro.setText("Autor do Livro:");

        jtxtfAutorLivro.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfAutorLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfAutorLivroActionPerformed(evt);
            }
        });

        PrncCmpAluno.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpAluno.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpAluno.setText("Preencha este campo");

        PrncCmpAutorLivro.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpAutorLivro.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpAutorLivro.setText("Preencha este campo");

        PrncCmpdataDevol.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpdataDevol.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpdataDevol.setText("Preencha este campo");

        PrncCmpdataAlug.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpdataAlug.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpdataAlug.setText("Preencha este campo");

        PrncCmpTitleLivro.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpTitleLivro.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpTitleLivro.setText("Preencha este campo");

        PrncCmpTurma.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpTurma.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpTurma.setText("Selecione um item");

        javax.swing.GroupLayout jpnFundoLayout = new javax.swing.GroupLayout(jpnFundo);
        jpnFundo.setLayout(jpnFundoLayout);
        jpnFundoLayout.setHorizontalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlblNomeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlblDataAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(810, 810, 810))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jlblTurma, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlblTituloLivro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                            .addComponent(jlblAutorLivro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(38, 38, 38)
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                                .addComponent(jformattedtxtfDataAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jlblDataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                                .addComponent(PrncCmpdataAlug, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(PrncCmpdataDevol, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jformattedtxtfDataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jtxtfNomeAluno, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(PrncCmpAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcboxTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jtxtfAutorLivro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
                                            .addComponent(jtxtfTituloLivro, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addGap(18, 18, 18)
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(PrncCmpTitleLivro)
                                            .addComponent(PrncCmpAutorLivro)))
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGap(155, 155, 155)
                                        .addComponent(PrncCmpTurma)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jpnFundoLayout.setVerticalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jlblTitulo)
                .addGap(127, 127, 127)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblNomeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jtxtfNomeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(PrncCmpAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jlblTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jcboxTurma, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                        .addComponent(PrncCmpTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtxtfTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PrncCmpTitleLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jlblAutorLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jtxtfAutorLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(PrncCmpAutorLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlblDataAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jformattedtxtfDataAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                            .addComponent(jlblDataDevolucao, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                            .addComponent(jformattedtxtfDataDevolucao, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PrncCmpdataAlug, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PrncCmpdataDevol, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(81, 81, 81)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64))
        );

        jtxtfNomeAluno.getAccessibleContext().setAccessibleName("Nome do Aluno");
        jtxtfNomeAluno.getAccessibleContext().setAccessibleDescription("Digite Aqui o nome do aluno");
        jtxtfTituloLivro.getAccessibleContext().setAccessibleName("Titulo do livro");
        jtxtfTituloLivro.getAccessibleContext().setAccessibleDescription("Digite aqui o título do livro");
        jformattedtxtfDataDevolucao.getAccessibleContext().setAccessibleName("data de Devolução");
        jformattedtxtfDataDevolucao.getAccessibleContext().setAccessibleDescription("data em que o livro deve ser devolvido (formato: Ano, mês e dia)");
        jformattedtxtfDataAluguel.getAccessibleContext().setAccessibleName("Data de aluguel");
        jformattedtxtfDataAluguel.getAccessibleContext().setAccessibleDescription("Digite a data em que o aluguel foi feito (formato: Ano, mês e dia)");
        jcboxTurma.getAccessibleContext().setAccessibleName("Turma do Aluno");
        jcboxTurma.getAccessibleContext().setAccessibleDescription("use as setas de cima e baixo para mudar a turma do aluno (ordem: primeiro, segundo e terceiro, informática, enfermagem e guia)");
        jtxtfAutorLivro.getAccessibleContext().setAccessibleName("Nome do Autor do livro");
        jtxtfAutorLivro.getAccessibleContext().setAccessibleDescription("Digite aqui o nome do autor do livro");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setBounds(125, 50, 1151, 768);
    }// </editor-fold>//GEN-END:initComponents

    private void jtxtfNomeAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfNomeAlunoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfNomeAlunoActionPerformed

    private void jtxtfTituloLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfTituloLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfTituloLivroActionPerformed

    private void jformattedtxtfDataAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jformattedtxtfDataAluguelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jformattedtxtfDataAluguelActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if(jtxtfNomeAluno.getText().length() > 0) {
            PrncCmpAluno.setVisible(false);
        }else{
            PrncCmpAluno.setVisible(true);
        }
        if(jtxtfTituloLivro.getText().length() > 0) {
            PrncCmpTitleLivro.setVisible(false);
        }else{
            PrncCmpTitleLivro.setVisible(true);
        }
        if(jtxtfAutorLivro.getText().length() > 0) {
            PrncCmpAutorLivro.setVisible(false);
        }else{
            PrncCmpAutorLivro.setVisible(true);
        }
        if(jformattedtxtfDataAluguel.getText().equals("    -  -  ")){
            PrncCmpdataAlug.setVisible(true);
        }else{
            PrncCmpdataAlug.setVisible(false);
        }
        if(jformattedtxtfDataDevolucao.getText().equals("    -  -  ")){
            PrncCmpdataDevol.setVisible(true);
        }else{
            PrncCmpdataDevol.setVisible(false);
        }
        if(jcboxTurma.getSelectedItem().equals("Selecionar")){
            PrncCmpTurma.setVisible(true);
        }else{
            PrncCmpTurma.setVisible(false);
        }

      if(verifDados.verificaVazio(jtxtfNomeAluno.getText(), jtxtfTituloLivro.getText(), jtxtfAutorLivro.getText())){
            if(!jformattedtxtfDataDevolucao.getText().equals("    -  -  ") && !jformattedtxtfDataAluguel.getText().equals("    -  -  ")){
                if(!jcboxTurma.getSelectedItem().equals("Selecionar")){
                   
                java.sql.Date date1 = java.sql.Date.valueOf(jformattedtxtfDataAluguel.getText());
                java.sql.Date date2 = java.sql.Date.valueOf(jformattedtxtfDataDevolucao.getText());
                
                alug.est.setNome(jtxtfNomeAluno.getText());
                alug.est.setTurma(jcboxTurma.getItemAt(jcboxTurma.getSelectedIndex()));
                alug.book.setNome(jtxtfTituloLivro.getText());
                alug.book.setAutor(jtxtfAutorLivro.getText());    
                alug.setLocacao(date1);
                alug.setDevolucao(date2);
                if(add.registraAluno(alug)){
                    jtxtfNomeAluno.setText("");
                    jtxtfTituloLivro.setText("");
                    jtxtfAutorLivro.setText("");
                    jformattedtxtfDataDevolucao.setText("");
                    jformattedtxtfDataAluguel.setText("");
                    jcboxTurma.setSelectedItem("Selecionar");
                    JOptionPane.showMessageDialog(null, "Aluguel registrado");
                    this.dispose();
                }else{
                    JOptionPane.showMessageDialog(null, "Falha ao registrar aluguel");
                }
               }else{
                    JOptionPane.showMessageDialog(null, "Selecione uma turma!");
               }       
                
            }else{
                JOptionPane.showMessageDialog(null, "Datas não preenchidas");
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Você deve preencher todos os campos");
        }
        //fim      
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        jtxtfNomeAluno.setText("");
        jtxtfTituloLivro.setText("");
        jtxtfAutorLivro.setText("");
        jformattedtxtfDataDevolucao.setText("");
        jformattedtxtfDataAluguel.setText("");
        jcboxTurma.setSelectedItem("Selecionar");
        PrncCmpAluno.setVisible(false);
        PrncCmpTitleLivro.setVisible(false);
        PrncCmpAutorLivro.setVisible(false);
        PrncCmpdataAlug.setVisible(false);
        PrncCmpdataDevol.setVisible(false);
        PrncCmpTurma.setVisible(false);

        anterior.focoNoMenu();
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void jcboxTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcboxTurmaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcboxTurmaActionPerformed

    private void jtxtfAutorLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfAutorLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfAutorLivroActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel PrncCmpAluno;
    private javax.swing.JLabel PrncCmpAutorLivro;
    private javax.swing.JLabel PrncCmpTitleLivro;
    private javax.swing.JLabel PrncCmpTurma;
    private javax.swing.JLabel PrncCmpdataAlug;
    private javax.swing.JLabel PrncCmpdataDevol;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> jcboxTurma;
    private javax.swing.JFormattedTextField jformattedtxtfDataAluguel;
    private javax.swing.JFormattedTextField jformattedtxtfDataDevolucao;
    private javax.swing.JLabel jlblAutorLivro;
    private javax.swing.JLabel jlblDataAluguel;
    private javax.swing.JLabel jlblDataDevolucao;
    private javax.swing.JLabel jlblNomeAluno;
    private javax.swing.JLabel jlblTitulo;
    private javax.swing.JLabel jlblTituloLivro;
    private javax.swing.JLabel jlblTurma;
    private javax.swing.JPanel jpnFundo;
    private javax.swing.JTextField jtxtfAutorLivro;
    private javax.swing.JTextField jtxtfNomeAluno;
    private javax.swing.JTextField jtxtfTituloLivro;
    // End of variables declaration//GEN-END:variables
}
