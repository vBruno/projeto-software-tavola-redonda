package visual;
import controle.EditarAluguel;
import javax.swing.JOptionPane;
import modelo.Aluguel;
import utilitarios.VerificaDados;
/**
 *
 * @author eduardahey
 */
public class EditarAlug extends javax.swing.JInternalFrame {

    /**
     * Creates new form EditarAlug
     */
    private TelaInicial anterior;
    public EditarAlug(TelaInicial anterior) {
        initComponents();
        PrncCmpAluno.setVisible(false);
        PrncCmpTitleLivro.setVisible(false);
        PrncCmpAutorLivro.setVisible(false);
        PrncCmpdataAlug.setVisible(false);
        PrncCmpdataDevol.setVisible(false);
        PrncCmpID.setVisible(false);
        PrncCmpTurma.setVisible(false);
        this.anterior = anterior;
    }
    //Fim do Método Construtor;
    EditarAluguel editr = new EditarAluguel();
    Aluguel alug = new Aluguel();
    VerificaDados verifDados = new VerificaDados();    
    
    @SuppressWarnings("unchecked")
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnFundo = new javax.swing.JPanel();
        jlblNomeAluno = new javax.swing.JLabel();
        jlblTurma = new javax.swing.JLabel();
        jlblTituloLivro = new javax.swing.JLabel();
        jlblDataAluguel = new javax.swing.JLabel();
        jlblDataDevolucao = new javax.swing.JLabel();
        jtxtfNomeAluno = new javax.swing.JTextField();
        jtxtfTituloLivro = new javax.swing.JTextField();
        jformattedtxtfDataDevolucao = new javax.swing.JFormattedTextField();
        jformattedtxtfDataAluguel = new javax.swing.JFormattedTextField();
        btnEditar = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();
        jlblTitulo = new javax.swing.JLabel();
        jcboxTurma = new javax.swing.JComboBox<>();
        jlblAutorLivro = new javax.swing.JLabel();
        jtxtfAutorLivro = new javax.swing.JTextField();
        PrncCmpAluno = new javax.swing.JLabel();
        PrncCmpAutorLivro = new javax.swing.JLabel();
        PrncCmpdataDevol = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jtxtfIDAluguel = new javax.swing.JTextField();
        PrncCmpdataAlug = new javax.swing.JLabel();
        PrncCmpTitleLivro = new javax.swing.JLabel();
        PrncCmpID = new javax.swing.JLabel();
        PrncCmpTurma = new javax.swing.JLabel();
        btnConsultar = new javax.swing.JButton();

        setBorder(null);
        setTitle("Editar Aluguel de Livro");
        setMaximumSize(new java.awt.Dimension(2000, 1000));
        setMinimumSize(new java.awt.Dimension(100, 50));

        jpnFundo.setBackground(new java.awt.Color(239, 253, 251));
        jpnFundo.setMaximumSize(new java.awt.Dimension(1024, 500));
        jpnFundo.setMinimumSize(new java.awt.Dimension(1024, 500));

        jlblNomeAluno.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblNomeAluno.setText("Nome do Aluno:");

        jlblTurma.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblTurma.setText("Turma:");

        jlblTituloLivro.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblTituloLivro.setText("Título do Livro:");

        jlblDataAluguel.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblDataAluguel.setText("Data de Aluguel:");

        jlblDataDevolucao.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblDataDevolucao.setText("Data de Devolução:");

        jtxtfNomeAluno.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfNomeAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfNomeAlunoActionPerformed(evt);
            }
        });

        jtxtfTituloLivro.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfTituloLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfTituloLivroActionPerformed(evt);
            }
        });

        jformattedtxtfDataDevolucao.setBackground(new java.awt.Color(204, 204, 204));
        try {
            jformattedtxtfDataDevolucao.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-##-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        jformattedtxtfDataAluguel.setBackground(new java.awt.Color(204, 204, 204));
        try {
            jformattedtxtfDataAluguel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-##-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jformattedtxtfDataAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jformattedtxtfDataAluguelActionPerformed(evt);
            }
        });

        btnEditar.setBackground(new java.awt.Color(90, 207, 184));
        btnEditar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnEditar.setForeground(new java.awt.Color(255, 255, 255));
        btnEditar.setText("Editar");
        btnEditar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnFechar.setBackground(new java.awt.Color(90, 207, 184));
        btnFechar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnFechar.setForeground(new java.awt.Color(255, 255, 255));
        btnFechar.setText("Fechar");
        btnFechar.setToolTipText("");
        btnFechar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        jlblTitulo.setFont(new java.awt.Font("Monospaced", 1, 36)); // NOI18N
        jlblTitulo.setText("Editar Aluguel de Livro:");

        jcboxTurma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Selecionar", "1º Informática", "1º  Enfermagem", "1º Guia de Turismo", "2º Informática", "2º Enfermagem", "2º Guia de Turismo", "3º Informática", "3º Enfermagem", "3º Guia de Turismo" }));
        jcboxTurma.setToolTipText("");
        jcboxTurma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jcboxTurmaActionPerformed(evt);
            }
        });

        jlblAutorLivro.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jlblAutorLivro.setText("Autor do Livro:");

        jtxtfAutorLivro.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfAutorLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfAutorLivroActionPerformed(evt);
            }
        });

        PrncCmpAluno.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpAluno.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpAluno.setText("Preencha este campo");

        PrncCmpAutorLivro.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpAutorLivro.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpAutorLivro.setText("Preencha este campo");

        PrncCmpdataDevol.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpdataDevol.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpdataDevol.setText("Preencha este campo");

        jLabel1.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        jLabel1.setText("Id do Aluguel:");

        jtxtfIDAluguel.setBackground(new java.awt.Color(204, 204, 204));
        jtxtfIDAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtxtfIDAluguelActionPerformed(evt);
            }
        });

        PrncCmpdataAlug.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpdataAlug.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpdataAlug.setText("Preencha este campo");

        PrncCmpTitleLivro.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpTitleLivro.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpTitleLivro.setText("Preencha este campo");

        PrncCmpID.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpID.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpID.setText("Preencha este campo");

        PrncCmpTurma.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        PrncCmpTurma.setForeground(new java.awt.Color(255, 0, 51));
        PrncCmpTurma.setText("Selecione um item");

        btnConsultar.setBackground(new java.awt.Color(90, 207, 184));
        btnConsultar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnConsultar.setForeground(new java.awt.Color(255, 255, 255));
        btnConsultar.setText("Consultar");
        btnConsultar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnFundoLayout = new javax.swing.GroupLayout(jpnFundo);
        jpnFundo.setLayout(jpnFundoLayout);
        jpnFundoLayout.setHorizontalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlblNomeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlblDataAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(810, 810, 810))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jlblTurma, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jlblTituloLivro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                                .addComponent(jlblAutorLivro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(38, 38, 38)
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                                .addComponent(jformattedtxtfDataAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jlblDataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                                .addComponent(PrncCmpdataAlug, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(PrncCmpdataDevol, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jformattedtxtfDataDevolucao, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jtxtfNomeAluno, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(PrncCmpAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jpnFundoLayout.createSequentialGroup()
                                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jcboxTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jtxtfAutorLivro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
                                            .addComponent(jtxtfTituloLivro, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addGap(18, 18, 18)
                                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(PrncCmpTitleLivro)
                                            .addComponent(PrncCmpAutorLivro)))
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addComponent(jtxtfIDAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(PrncCmpID, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jpnFundoLayout.createSequentialGroup()
                                        .addGap(155, 155, 155)
                                        .addComponent(PrncCmpTurma)))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jpnFundoLayout.setVerticalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jlblTitulo)
                .addGap(57, 57, 57)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jtxtfIDAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PrncCmpID, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblNomeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jtxtfNomeAluno, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(PrncCmpAluno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jlblTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jcboxTurma, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                        .addComponent(PrncCmpTurma, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlblTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtxtfTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PrncCmpTitleLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(jlblAutorLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jtxtfAutorLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(PrncCmpAutorLivro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(36, 36, 36)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jlblDataAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpnFundoLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jformattedtxtfDataAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                            .addComponent(jlblDataDevolucao, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                            .addComponent(jformattedtxtfDataDevolucao, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PrncCmpdataAlug, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PrncCmpdataDevol, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(81, 81, 81)
                .addGroup(jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64))
        );

        jtxtfNomeAluno.getAccessibleContext().setAccessibleName("Nome do aluno");
        jtxtfNomeAluno.getAccessibleContext().setAccessibleDescription("Modifique o nome do aluno que deseja editar");
        jtxtfTituloLivro.getAccessibleContext().setAccessibleName("Titulo do livro");
        jtxtfTituloLivro.getAccessibleContext().setAccessibleDescription("modifique o titulo do livro");
        jformattedtxtfDataDevolucao.getAccessibleContext().setAccessibleName("data de devolução");
        jformattedtxtfDataDevolucao.getAccessibleContext().setAccessibleDescription("modifique a data de devolução (formato: ano mês e dia)");
        jformattedtxtfDataAluguel.getAccessibleContext().setAccessibleName("Data de aluguel");
        jformattedtxtfDataAluguel.getAccessibleContext().setAccessibleDescription("modifique a data de aluguel (formato: ano, mês e dia)");
        btnEditar.getAccessibleContext().setAccessibleDescription("");
        jcboxTurma.getAccessibleContext().setAccessibleName("Turma do aluno");
        jcboxTurma.getAccessibleContext().setAccessibleDescription("use as setas de cima e baixo para mudar a turma do aluno (ordem: primeiro, segundo e terceiro, informática, enfermagem e guia)");
        jtxtfAutorLivro.getAccessibleContext().setAccessibleName("Autor do livro");
        jtxtfAutorLivro.getAccessibleContext().setAccessibleDescription("modifique o autor do livro");
        jtxtfIDAluguel.getAccessibleContext().setAccessibleName("Id do aluguel");
        jtxtfIDAluguel.getAccessibleContext().setAccessibleDescription("Digite aqui o ID do aluguel que deseja editar");
        btnConsultar.getAccessibleContext().setAccessibleDescription("Clique para consultar um aluguel");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 27, Short.MAX_VALUE))
        );

        setBounds(125, 50, 1151, 768);
    }// </editor-fold>//GEN-END:initComponents

    private void jtxtfIDAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfIDAluguelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfIDAluguelActionPerformed

    private void jtxtfAutorLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfAutorLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfAutorLivroActionPerformed

    private void jcboxTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jcboxTurmaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jcboxTurmaActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        jtxtfIDAluguel.setText("");
        jtxtfNomeAluno.setText("");
        jtxtfTituloLivro.setText("");
        jtxtfAutorLivro.setText("");
        jformattedtxtfDataDevolucao.setText("");
        jformattedtxtfDataAluguel.setText("");
        jcboxTurma.setSelectedItem("Selecionar");
        PrncCmpAluno.setVisible(false);
        PrncCmpTitleLivro.setVisible(false);
        PrncCmpAutorLivro.setVisible(false);
        PrncCmpdataAlug.setVisible(false);
        PrncCmpdataDevol.setVisible(false);
        PrncCmpTurma.setVisible(false);
        anterior.focoNoMenu();
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if(jtxtfNomeAluno.getText().length() > 0) {
            PrncCmpAluno.setVisible(false);
        }else{
            PrncCmpAluno.setVisible(true);
        }
        if(jtxtfTituloLivro.getText().length() > 0) {
            PrncCmpTitleLivro.setVisible(false);
        }else{
            PrncCmpTitleLivro.setVisible(true);
        }
        if(jtxtfAutorLivro.getText().length() > 0) {
            PrncCmpAutorLivro.setVisible(false);
        }else{
            PrncCmpAutorLivro.setVisible(true);
        }
        if(jformattedtxtfDataAluguel.getText().equals("    -  -  ")){
            PrncCmpdataAlug.setVisible(true);
        }else{
            PrncCmpdataAlug.setVisible(false);
        }
        if(jformattedtxtfDataDevolucao.getText().equals("    -  -  ")){
            PrncCmpdataDevol.setVisible(true);
        }else{
            PrncCmpdataDevol.setVisible(false);
        }
        if(jtxtfIDAluguel.getText().length() > 0) {
            PrncCmpID.setVisible(false);
        }else{
            PrncCmpID.setVisible(true);
        }
        if(jcboxTurma.getSelectedItem().equals("Selecionar")){
            PrncCmpTurma.setVisible(true);
        }else{
            PrncCmpTurma.setVisible(false);
        }

        if(verifDados.verificaVazio(jtxtfNomeAluno.getText(), jtxtfTituloLivro.getText(), jtxtfAutorLivro.getText(), jtxtfIDAluguel.getText())){
            if(!jformattedtxtfDataDevolucao.getText().equals("    -  -  ") && !jformattedtxtfDataAluguel.getText().equals("    -  -  ")){
               if(!jcboxTurma.getSelectedItem().equals("Selecionar")){
                java.sql.Date date1 = java.sql.Date.valueOf(jformattedtxtfDataAluguel.getText());
                java.sql.Date date2 = java.sql.Date.valueOf(jformattedtxtfDataDevolucao.getText());

                alug.est.setNome(jtxtfNomeAluno.getText());
                alug.est.setTurma(jcboxTurma.getItemAt(jcboxTurma.getSelectedIndex()));
                alug.book.setNome(jtxtfTituloLivro.getText());
                alug.book.setAutor(jtxtfAutorLivro.getText());
                alug.setDevolucao(date2);
                alug.setLocacao(date1);
                int idc = Integer.parseInt(jtxtfIDAluguel.getText());
                alug.setIdAlug(idc);

                if(editr.EditarAluguel(alug)){
                    jtxtfIDAluguel.setText("");
                    jtxtfNomeAluno.setText("");
                    jtxtfTituloLivro.setText("");
                    jtxtfAutorLivro.setText("");
                    jformattedtxtfDataDevolucao.setText("");
                    jformattedtxtfDataAluguel.setText("");
                    jcboxTurma.setSelectedItem("Selecionar");
                    JOptionPane.showMessageDialog(null, "Aluguel Editado");
                    this.dispose();
                }else{
                    JOptionPane.showMessageDialog(null, "Falha ao editar aluguel");
                }
               }else{
                    JOptionPane.showMessageDialog(null, "Selecione uma turma!");
               }      

            }else{
                JOptionPane.showMessageDialog(null, "Datas não preenchidas");
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Você deve preencher todos os campos");
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void jformattedtxtfDataAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jformattedtxtfDataAluguelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jformattedtxtfDataAluguelActionPerformed

    private void jtxtfTituloLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfTituloLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfTituloLivroActionPerformed

    private void jtxtfNomeAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtxtfNomeAlunoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtxtfNomeAlunoActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        if(jtxtfIDAluguel.getText().equals("")){
            PrncCmpID.setVisible(true);
        }else{
            int id;
            int idc = Integer.parseInt(jtxtfIDAluguel.getText());
            Aluguel a = editr.consultar(idc);
            if(a == null){
                JOptionPane.showMessageDialog(null,"Aluguel não encontrado");
            }else{
                jtxtfNomeAluno.setText(a.est.getNome());
                jtxtfTituloLivro.setText(a.book.getNome());
                jtxtfAutorLivro.setText(a.book.getAutor());
                jformattedtxtfDataDevolucao.setText(a.getDevolucao().toString());
                jformattedtxtfDataAluguel.setText(a.getLocacao().toString());
                jcboxTurma.setSelectedItem(a.est.getTurma());
                PrncCmpID.setVisible(false);
                PrncCmpAluno.setVisible(false);
                PrncCmpTitleLivro.setVisible(false);
                PrncCmpAutorLivro.setVisible(false);
                PrncCmpdataAlug.setVisible(false);
                PrncCmpdataDevol.setVisible(false);
                PrncCmpTurma.setVisible(false);
            }
        }

    }//GEN-LAST:event_btnConsultarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel PrncCmpAluno;
    private javax.swing.JLabel PrncCmpAutorLivro;
    private javax.swing.JLabel PrncCmpID;
    private javax.swing.JLabel PrncCmpTitleLivro;
    private javax.swing.JLabel PrncCmpTurma;
    private javax.swing.JLabel PrncCmpdataAlug;
    private javax.swing.JLabel PrncCmpdataDevol;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JComboBox<String> jcboxTurma;
    private javax.swing.JFormattedTextField jformattedtxtfDataAluguel;
    private javax.swing.JFormattedTextField jformattedtxtfDataDevolucao;
    private javax.swing.JLabel jlblAutorLivro;
    private javax.swing.JLabel jlblDataAluguel;
    private javax.swing.JLabel jlblDataDevolucao;
    private javax.swing.JLabel jlblNomeAluno;
    private javax.swing.JLabel jlblTitulo;
    private javax.swing.JLabel jlblTituloLivro;
    private javax.swing.JLabel jlblTurma;
    private javax.swing.JPanel jpnFundo;
    private javax.swing.JTextField jtxtfAutorLivro;
    private javax.swing.JTextField jtxtfIDAluguel;
    private javax.swing.JTextField jtxtfNomeAluno;
    private javax.swing.JTextField jtxtfTituloLivro;
    // End of variables declaration//GEN-END:variables
}
