package visual;
import controle.Alugueis;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

 
public class Expirados extends javax.swing.JInternalFrame {

    Alugueis DAO;
    ImageIcon icone = new ImageIcon(getClass().getResource("imgs/trash.png")); //adicionar a imagem no JOptionPane
    UIManager UI; //importar classe de personalização do JOptionPane
    //Método Construtor
    private TelaInicial anterior;
    public Expirados(TelaInicial anterior) {
        initComponents();
        DAO = new Alugueis();        
        this.alugueisExpirados(date1);
        this.anterior = anterior;
    }
    //Fim do Método Construtor
    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String formattedDate = simpleDateFormat.format(date);
    java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);

    
    
    public void alugueisExpirados(java.sql.Date date1){
        try{
            Vector cabecalho = new Vector();
            cabecalho.add("Aluno");
            cabecalho.add("Livro");
            cabecalho.add("Data Devolução");
            cabecalho.add("Cod ALuguel");
            
            DefaultTableModel table = new DefaultTableModel(DAO.popularExpirados(date1), cabecalho);
            tabelaExpirados.setModel(table);
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaExpirados = new javax.swing.JTable();
        txtCodAluguel = new javax.swing.JTextField();
        btnApagarAluguel = new javax.swing.JButton();
        jtPesquisa = new javax.swing.JTextField();
        iconPesquisar = new javax.swing.JLabel();
        TituloExpirados = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnFechar = new javax.swing.JButton();

        setBackground(new java.awt.Color(239, 253, 251));
        setBorder(null);
        setTitle("Livros Expirados");

        tabelaExpirados.setBackground(new java.awt.Color(90, 207, 184));
        tabelaExpirados.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        tabelaExpirados.setForeground(new java.awt.Color(255, 255, 255));
        tabelaExpirados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Aluno", "Livro", "Data Devolução", "Cod Aluguel"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaExpirados.setRowHeight(25);
        tabelaExpirados.getTableHeader().setReorderingAllowed(false);
        tabelaExpirados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaExpiradosMouseClicked(evt);
            }
        });
        tabelaExpirados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabelaExpiradosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaExpirados);
        tabelaExpirados.getAccessibleContext().setAccessibleName("Tabela");
        tabelaExpirados.getAccessibleContext().setAccessibleDescription("Tabela com nome, livro, data de devolução e id do aluguel das pessoas com alugueis expirados");
        tabelaExpirados.getAccessibleContext().setAccessibleParent(tabelaExpirados);

        txtCodAluguel.setBackground(new java.awt.Color(108, 191, 197));
        txtCodAluguel.setForeground(new java.awt.Color(255, 255, 255));
        txtCodAluguel.setBorder(null);

        btnApagarAluguel.setBackground(new java.awt.Color(255, 0, 0));
        btnApagarAluguel.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnApagarAluguel.setForeground(new java.awt.Color(255, 255, 255));
        btnApagarAluguel.setText("Excluir Aluguel");
        btnApagarAluguel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnApagarAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApagarAluguelActionPerformed(evt);
            }
        });

        jtPesquisa.setBackground(new java.awt.Color(108, 191, 197));
        jtPesquisa.setForeground(new java.awt.Color(255, 255, 255));
        jtPesquisa.setBorder(null);
        jtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtPesquisaKeyReleased(evt);
            }
        });

        iconPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/search.png"))); // NOI18N

        TituloExpirados.setFont(new java.awt.Font("Monospaced", 1, 28)); // NOI18N
        TituloExpirados.setText("Aluguéis Expirados");

        jLabel2.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        jLabel2.setText("Excluir Aluguel");

        btnFechar.setBackground(new java.awt.Color(90, 207, 184));
        btnFechar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnFechar.setForeground(new java.awt.Color(255, 255, 255));
        btnFechar.setText("Fechar");
        btnFechar.setToolTipText("");
        btnFechar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TituloExpirados)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 788, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(iconPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(89, 89, 89)
                                .addComponent(txtCodAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(121, 121, 121)
                                .addComponent(jLabel2))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(149, 149, 149)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnApagarAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                                    .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(696, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(222, 222, 222)
                        .addComponent(jLabel2)
                        .addGap(51, 51, 51)
                        .addComponent(txtCodAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(btnApagarAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(TituloExpirados, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(iconPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(746, Short.MAX_VALUE))
        );

        txtCodAluguel.getAccessibleContext().setAccessibleName("Campo de texto");
        txtCodAluguel.getAccessibleContext().setAccessibleDescription("Campo para digitar o ID do aluguel para excluí-lo");
        txtCodAluguel.getAccessibleContext().setAccessibleParent(txtCodAluguel);
        btnApagarAluguel.getAccessibleContext().setAccessibleDescription("Botão para apagar aluguel");
        btnApagarAluguel.getAccessibleContext().setAccessibleParent(btnApagarAluguel);
        jtPesquisa.getAccessibleContext().setAccessibleName("Barra de pesquisa");
        jtPesquisa.getAccessibleContext().setAccessibleDescription("Pesquise o aluguel pelo nome do aluno");

        setBounds(125, 50, 1900, 1400);
    }// </editor-fold>//GEN-END:initComponents

    private void btnApagarAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApagarAluguelActionPerformed
        Alugueis aluguel = new Alugueis();
        int codigo = Integer.parseInt(txtCodAluguel.getText().trim());
        
        UI = null;
        UI.put("Button.background", new Color(108,191,197));
        UI.put("Button.foreground", new Color(255, 255, 255));
        UI.put("OptionPane.background", new Color(239,253,251));
        UI.put("Panel.background", new Color(239,253,251));
        if(JOptionPane.showConfirmDialog(this, "Deseja realmente remover o aluguel?", "Remover Aluguel", JOptionPane.ERROR_MESSAGE, JOptionPane.YES_NO_OPTION, icone) == JOptionPane.YES_OPTION){
            if(aluguel.deletarAluguel(codigo)){
                this.alugueisExpirados(date1);
                txtCodAluguel.setText("");
            }
        }
    }//GEN-LAST:event_btnApagarAluguelActionPerformed

    private void jtPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtPesquisaKeyReleased
        try{
            Vector cabecalho = new Vector();
            cabecalho.add("Aluno");
            cabecalho.add("Livro");
            cabecalho.add("Data Devolução");
            cabecalho.add("Cod ALuguel");

            if(!jtPesquisa.getText().equals("")){
                DefaultTableModel nv = new DefaultTableModel(DAO.PesquisarExp(jtPesquisa.getText().trim(), date1), cabecalho);
                tabelaExpirados.setModel(nv);
            }else{
                this.alugueisExpirados(date1);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jtPesquisaKeyReleased

    private void tabelaExpiradosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaExpiradosMouseClicked
        if(tabelaExpirados.getSelectedRow() != -1){
            txtCodAluguel.setText(tabelaExpirados.getValueAt(tabelaExpirados.getSelectedRow(), 3).toString());
        }
    }//GEN-LAST:event_tabelaExpiradosMouseClicked

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        anterior.focoNoMenu();
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void tabelaExpiradosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabelaExpiradosKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_SPACE){
            txtCodAluguel.setText(tabelaExpirados.getValueAt(tabelaExpirados.getSelectedRow(), 3).toString());
            txtCodAluguel.requestFocus();
        }  
    }//GEN-LAST:event_tabelaExpiradosKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel TituloExpirados;
    private javax.swing.JButton btnApagarAluguel;
    private javax.swing.JButton btnFechar;
    private javax.swing.JLabel iconPesquisar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jtPesquisa;
    private javax.swing.JTable tabelaExpirados;
    private javax.swing.JTextField txtCodAluguel;
    // End of variables declaration//GEN-END:variables

}
