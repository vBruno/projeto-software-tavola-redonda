
package visual;
 // @author lavigne
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;



public class Alugueis extends javax.swing.JInternalFrame {

   //Método Construtor
    controle.Alugueis DAO;
    UIManager UI; //importar classe de personalização do JOptionPane
    ImageIcon icone = new ImageIcon("/home/brunin/NetBeansProjects/TavRed/src/imgs/trash.png"); //adicionar a imagem no JOptionPane

    //Método Construtor
    private TelaInicial anterior;
    public Alugueis(TelaInicial anterior) {
        initComponents();
        DAO = new controle.Alugueis();        
        this.livrosAlugados(date1);
        this.anterior = anterior;
    }
    //Fim do Método Construtor
    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String formattedDate = simpleDateFormat.format(date);
    java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);//Fim do método Construtor;
    
    public void livrosAlugados(java.sql.Date date1){
        try{
            Vector cabecalho = new Vector();
            cabecalho.add("Aluno");
            cabecalho.add("Livro");
            cabecalho.add("Data de Devolução");
            cabecalho.add("Id do ALuguel");
            
            DefaultTableModel table;
            table = new DefaultTableModel(DAO.popularAlugados(date1), cabecalho);
            TabelaDeAlugados.setModel(table);
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TituloAlugueis = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TabelaDeAlugados = new javax.swing.JTable();
        jtPesquisa = new javax.swing.JTextField();
        iconPesquisar = new javax.swing.JLabel();
        txtCodAluguel = new javax.swing.JTextField();
        btnApagarAluguel = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnFechar = new javax.swing.JButton();

        setBackground(new java.awt.Color(239, 253, 251));
        setBorder(null);
        setTitle("Livros alugados");
        setVisible(true);

        TituloAlugueis.setFont(new java.awt.Font("Monospaced", 1, 28)); // NOI18N
        TituloAlugueis.setText("Aluguéis");

        TabelaDeAlugados.setBackground(new java.awt.Color(90, 207, 184));
        TabelaDeAlugados.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        TabelaDeAlugados.setForeground(new java.awt.Color(255, 255, 255));
        TabelaDeAlugados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Aluno", "Livro", "Data de Devolução", "Id do aluguel"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        TabelaDeAlugados.setMaximumSize(new java.awt.Dimension(2147483647, 0));
        TabelaDeAlugados.setMinimumSize(new java.awt.Dimension(60, 0));
        TabelaDeAlugados.setRowHeight(25);
        TabelaDeAlugados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabelaDeAlugadosMouseClicked(evt);
            }
        });
        TabelaDeAlugados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                TabelaDeAlugadosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                TabelaDeAlugadosKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(TabelaDeAlugados);
        TabelaDeAlugados.getAccessibleContext().setAccessibleName("Tabela de alugueis");
        TabelaDeAlugados.getAccessibleContext().setAccessibleDescription("Tabela onde são apresentados o nome do aluno, livro, data de devolução e Id dos alugueis");
        TabelaDeAlugados.getAccessibleContext().setAccessibleParent(TabelaDeAlugados);

        jtPesquisa.setBackground(new java.awt.Color(108, 191, 197));
        jtPesquisa.setBorder(null);
        jtPesquisa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtPesquisaActionPerformed(evt);
            }
        });
        jtPesquisa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jtPesquisaKeyReleased(evt);
            }
        });

        iconPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/search.png"))); // NOI18N

        txtCodAluguel.setBackground(new java.awt.Color(108, 191, 197));
        txtCodAluguel.setBorder(null);
        txtCodAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodAluguelActionPerformed(evt);
            }
        });
        txtCodAluguel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodAluguelKeyPressed(evt);
            }
        });

        btnApagarAluguel.setBackground(new java.awt.Color(255, 0, 0));
        btnApagarAluguel.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnApagarAluguel.setForeground(new java.awt.Color(255, 255, 255));
        btnApagarAluguel.setText("Excluir Aluguel");
        btnApagarAluguel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnApagarAluguel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnApagarAluguelActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Monospaced", 1, 24)); // NOI18N
        jLabel2.setText("Excluir Aluguel");

        btnFechar.setBackground(new java.awt.Color(90, 207, 184));
        btnFechar.setFont(new java.awt.Font("Monospaced", 1, 14)); // NOI18N
        btnFechar.setForeground(new java.awt.Color(255, 255, 255));
        btnFechar.setText("Fechar");
        btnFechar.setToolTipText("");
        btnFechar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TituloAlugueis)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 788, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(89, 89, 89)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCodAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(btnApagarAluguel, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                                            .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(118, 118, 118)
                                .addComponent(jLabel2))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(iconPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 690, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(TituloAlugueis, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(iconPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 514, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)
                        .addComponent(txtCodAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(35, 35, 35)
                        .addComponent(btnApagarAluguel, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(748, Short.MAX_VALUE))
        );

        jtPesquisa.getAccessibleContext().setAccessibleName("Pesquisar alugueis");
        jtPesquisa.getAccessibleContext().setAccessibleDescription("Pesquise os alugueis pelo nome do aluno");
        jtPesquisa.getAccessibleContext().setAccessibleParent(jtPesquisa);
        txtCodAluguel.getAccessibleContext().setAccessibleName("Campo de ID");
        txtCodAluguel.getAccessibleContext().setAccessibleDescription("Digite aqui o id do aluguel que deseja excluir");

        setBounds(125, 50, 1900, 1402);
    }// </editor-fold>//GEN-END:initComponents

    private void jtPesquisaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtPesquisaKeyReleased
        try{
            Vector cabecalho = new Vector();
            cabecalho.add("Aluno");
            cabecalho.add("Livro");
            cabecalho.add("Data Devolução");
            cabecalho.add("Cod ALuguel");

            if(!jtPesquisa.getText().equals("")){
                DefaultTableModel nv = new DefaultTableModel(DAO.PesquisarAlug(jtPesquisa.getText().trim(), date1), cabecalho);
                TabelaDeAlugados.setModel(nv);
            }else{
                this.livrosAlugados(date1);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jtPesquisaKeyReleased

    private void jtPesquisaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtPesquisaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtPesquisaActionPerformed

    private void btnApagarAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnApagarAluguelActionPerformed
        controle.Alugueis aluguel = new controle.Alugueis();
        int codigo = Integer.parseInt(txtCodAluguel.getText().trim());

        UI = null;
        UI.put("Button.background", new Color(108,191,197));
        UI.put("Button.foreground", new Color(255, 255, 255));
        UI.put("OptionPane.background", new Color(239,253,251));
        UI.put("Panel.background", new Color(239,253,251));
        if(JOptionPane.showConfirmDialog(this, "Deseja realmente remover o aluguel?", "Remover Aluguel", JOptionPane.ERROR_MESSAGE, JOptionPane.YES_NO_OPTION, icone) == JOptionPane.YES_OPTION){
            if(aluguel.deletarAluguel(codigo)){
                this.livrosAlugados(date1);
                txtCodAluguel.setText("");
            }
        }
    }//GEN-LAST:event_btnApagarAluguelActionPerformed

    private void TabelaDeAlugadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabelaDeAlugadosMouseClicked
        if(TabelaDeAlugados.getSelectedRow() != -1){
            txtCodAluguel.setText(TabelaDeAlugados.getValueAt(TabelaDeAlugados.getSelectedRow(), 3).toString());
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_TabelaDeAlugadosMouseClicked

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        anterior.focoNoMenu();
        this.dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void txtCodAluguelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodAluguelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodAluguelActionPerformed

    private void txtCodAluguelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodAluguelKeyPressed
        if(evt.getKeyChar() == 'e'){
            if(evt.isControlDown()){
                if(TabelaDeAlugados.getSelectedRow() != -1){
                    txtCodAluguel.setText(TabelaDeAlugados.getValueAt(TabelaDeAlugados.getSelectedRow(), 3).toString());
                    txtCodAluguel.requestFocus();
                }
            }
        }
    }//GEN-LAST:event_txtCodAluguelKeyPressed

    private void TabelaDeAlugadosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TabelaDeAlugadosKeyReleased
      
    }//GEN-LAST:event_TabelaDeAlugadosKeyReleased

    private void TabelaDeAlugadosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TabelaDeAlugadosKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_SPACE){
            txtCodAluguel.setText(TabelaDeAlugados.getValueAt(TabelaDeAlugados.getSelectedRow(), 3).toString());
            txtCodAluguel.requestFocus();
        }
    }//GEN-LAST:event_TabelaDeAlugadosKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable TabelaDeAlugados;
    private javax.swing.JLabel TituloAlugueis;
    private javax.swing.JButton btnApagarAluguel;
    private javax.swing.JButton btnFechar;
    private javax.swing.JLabel iconPesquisar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jtPesquisa;
    private javax.swing.JTextField txtCodAluguel;
    // End of variables declaration//GEN-END:variables
}
