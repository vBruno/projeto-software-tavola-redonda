package visual;

//@author lavigne

import java.awt.Color;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class TelaInicial extends javax.swing.JFrame {
    //Instancias das classes jInternalFrame
    AdicionarAluguel AddAlug = new AdicionarAluguel(this);
    Alugueis Alug = new Alugueis(this);
    Expirados Expird = new Expirados(this);
    EditarAlug Edit = new EditarAlug(this);
    UIManager UI;
    //Fim das instancias
    
    //Método Construtor;
    public TelaInicial() {
        initComponents();
        this.btnAddAlug.requestFocus();
        //Deixa a tela maximizada;
        setExtendedState(TelaInicial.MAXIMIZED_BOTH); 
    }
    //Fim do  Método Construtor;
    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    String formattedDate = simpleDateFormat.format(date);
    java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpnFundo = new javax.swing.JPanel();
        jpnMenu = new javax.swing.JPanel();
        btnExpird = new javax.swing.JButton();
        btnAlugueis = new javax.swing.JButton();
        btnAddAlug = new javax.swing.JButton();
        btnEdiAlug = new javax.swing.JButton();
        btnSair = new javax.swing.JButton();
        jpnConteiner = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Távola Redonda");
        setResizable(false);

        jpnFundo.setBackground(new java.awt.Color(255, 255, 255));

        jpnMenu.setBackground(new java.awt.Color(90, 207, 184));
        jpnMenu.setPreferredSize(new java.awt.Dimension(60, 624));

        btnExpird.setBackground(new java.awt.Color(90, 207, 184));
        btnExpird.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconExpirados.png"))); // NOI18N
        btnExpird.setBorder(null);
        btnExpird.setBorderPainted(false);
        btnExpird.setContentAreaFilled(false);
        btnExpird.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExpirdActionPerformed(evt);
            }
        });

        btnAlugueis.setBackground(new java.awt.Color(90, 207, 184));
        btnAlugueis.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconAlugueis.png"))); // NOI18N
        btnAlugueis.setBorder(null);
        btnAlugueis.setBorderPainted(false);
        btnAlugueis.setContentAreaFilled(false);
        btnAlugueis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlugueisActionPerformed(evt);
            }
        });

        btnAddAlug.setBackground(new java.awt.Color(90, 207, 184));
        btnAddAlug.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconAdicionar.png"))); // NOI18N
        btnAddAlug.setBorder(null);
        btnAddAlug.setBorderPainted(false);
        btnAddAlug.setContentAreaFilled(false);
        btnAddAlug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddAlugActionPerformed(evt);
            }
        });

        btnEdiAlug.setBackground(new java.awt.Color(90, 207, 184));
        btnEdiAlug.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/editarAluguel(1).png"))); // NOI18N
        btnEdiAlug.setBorder(null);
        btnEdiAlug.setBorderPainted(false);
        btnEdiAlug.setContentAreaFilled(false);
        btnEdiAlug.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnEdiAlugMouseClicked(evt);
            }
        });
        btnEdiAlug.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEdiAlugActionPerformed(evt);
            }
        });

        btnSair.setBackground(new java.awt.Color(90, 207, 184));
        btnSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/IconSair.png"))); // NOI18N
        btnSair.setBorder(null);
        btnSair.setBorderPainted(false);
        btnSair.setContentAreaFilled(false);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpnMenuLayout = new javax.swing.GroupLayout(jpnMenu);
        jpnMenu.setLayout(jpnMenuLayout);
        jpnMenuLayout.setHorizontalGroup(
            jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnMenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnMenuLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddAlug, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnExpird, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jpnMenuLayout.createSequentialGroup()
                        .addGroup(jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSair)
                            .addComponent(btnEdiAlug)
                            .addComponent(btnAlugueis))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpnMenuLayout.setVerticalGroup(
            jpnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpnMenuLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(btnAddAlug, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                .addGap(78, 78, 78)
                .addComponent(btnEdiAlug, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                .addGap(78, 78, 78)
                .addComponent(btnAlugueis, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                .addGap(78, 78, 78)
                .addComponent(btnExpird, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                .addGap(78, 78, 78)
                .addComponent(btnSair, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(40, 40, 40))
        );

        btnExpird.getAccessibleContext().setAccessibleName("Acessar alugueis de livros expirados");
        btnAlugueis.getAccessibleContext().setAccessibleName("Acessar alugueis de livro já salvos");
        btnAddAlug.getAccessibleContext().setAccessibleName("Adicionar novo aluguel de livro");
        btnAddAlug.getAccessibleContext().setAccessibleDescription("");
        btnEdiAlug.getAccessibleContext().setAccessibleName("Editar aluguel de livro");
        btnSair.getAccessibleContext().setAccessibleName("Log out");
        btnSair.getAccessibleContext().setAccessibleDescription("Clique aqui para sair da aplicação");

        jpnConteiner.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jpnConteinerLayout = new javax.swing.GroupLayout(jpnConteiner);
        jpnConteiner.setLayout(jpnConteinerLayout);
        jpnConteinerLayout.setHorizontalGroup(
            jpnConteinerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 955, Short.MAX_VALUE)
        );
        jpnConteinerLayout.setVerticalGroup(
            jpnConteinerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jpnFundoLayout = new javax.swing.GroupLayout(jpnFundo);
        jpnFundo.setLayout(jpnFundoLayout);
        jpnFundoLayout.setHorizontalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addComponent(jpnMenu, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jpnConteiner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jpnFundoLayout.setVerticalGroup(
            jpnFundoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 656, Short.MAX_VALUE)
            .addGroup(jpnFundoLayout.createSequentialGroup()
                .addComponent(jpnConteiner, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpnFundo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //Enventos de Click dos Botões do menu;
    private void btnExpirdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExpirdActionPerformed
        jpnConteiner.add(Expird);
        try {
            Expird.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        Expird.alugueisExpirados(date1);
        AddAlug.setVisible(false);
        Edit.setVisible(false);
        Alug.setVisible(false);
        Expird.setVisible(true);
    }//GEN-LAST:event_btnExpirdActionPerformed

    private void btnAddAlugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddAlugActionPerformed
        //Adiciona a tela de Adicionar Aluguel ao painel quando o Botão de Adicionar é clicado;
        jpnConteiner.add(AddAlug);
        try {
            AddAlug.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        AddAlug.setVisible(true);
        Alug.setVisible(false);
        Expird.setVisible(false);
        Edit.setVisible(false);
    }//GEN-LAST:event_btnAddAlugActionPerformed
    public void focoNoMenu(){
        this.btnAddAlug.requestFocus();
    }
    private void btnAlugueisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlugueisActionPerformed
        jpnConteiner.add(Alug);
        try {
            Alug.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        Alug.livrosAlugados(date1);
        AddAlug.setVisible(false);
        Alug.setVisible(true);
        Expird.setVisible(false);
        Edit.setVisible(false);
    }//GEN-LAST:event_btnAlugueisActionPerformed

    private void btnEdiAlugActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEdiAlugActionPerformed

        jpnConteiner.add(Edit);
        try {
            Edit.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
        Edit.setVisible(true);
        AddAlug.setVisible(false);
        Alug.setVisible(false);
        Expird.setVisible(false);
    }//GEN-LAST:event_btnEdiAlugActionPerformed

    private void btnEdiAlugMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEdiAlugMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEdiAlugMouseClicked

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        UI = null;
        UI.put("Button.background", new Color(108,191,197));
        UI.put("Button.foreground", new Color(255, 255, 255));
        UI.put("OptionPane.background", new Color(239,253,251));
        UI.put("Panel.background", new Color(239,253,251));
        
        if(JOptionPane.showConfirmDialog(this, "Deseja fazer logout?", "Louout", JOptionPane.ERROR_MESSAGE, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
            Login obj = new Login();
            obj.setVisible(true);
            this.dispose();
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSairActionPerformed
    //Fim dos eventos de botões;
    
    
    /*
       Método  main temporário (será apagado quando toda a aplicação estiver montada, pois é necessário que haja um
       arquivo execultável para fazer  testes);
    */
    //Fim do método main;
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddAlug;
    private javax.swing.JButton btnAlugueis;
    private javax.swing.JButton btnEdiAlug;
    private javax.swing.JButton btnExpird;
    private javax.swing.JButton btnSair;
    public javax.swing.JPanel jpnConteiner;
    private javax.swing.JPanel jpnFundo;
    private javax.swing.JPanel jpnMenu;
    // End of variables declaration//GEN-END:variables
}
